<?php

/**
 * @file
 * Handle main functionality for interfacing with usergroups.
 */


/**
 * Get the titles of groups.
 *
 * @param $gtype
 *   (optional) The id of the group type to get the title for.
 *
 * @return
 *   String title if $gtype is passed in, otherwise an array of groups
 *   currently defined in the system keyed by the group type id.
 */
function usergroups_api_get_group_titles($gtype = NULL) {
  $titles = array();
  $gtypes = _usergroups_api_get_gtypes($gtype);
  foreach ($gtypes as $gtyp => $group) {
    // Only add unhidden groups to the array.
    if ($gtype || empty($group['hidden'])) {
      $titles[$gtyp] = $group['title'];
    }
  }
  return $gtype ? (isset($titles[$gtype]) ? $titles[$gtype] : '') : $titles;
}

/**
 * Helper function to get a list of all possible groups for a user.
 *
 * @param $account
 *   The user object to load the list of possible groups for.
 * @param $gtype
 *   (optional) The specific group type to limit the user groups to.
 *
 * @return
 *   An array of usergroups that the user is part of keyed by the group_id.
 */
function usergroups_api_get_all_user_group_options($account, $gtype = NULL) {
  $gtypes =_usergroups_api_get_gtypes($gtype);
  $groups = array();
  foreach ($gtypes as $gtyp => $group) {
    $function = $group['module'] .'_usergroups_api_'. $gtyp .'_specific_groups';
    if (_usergroups_api_include_file($group, $function)) {
      $gs = $function($account);
      // Make sure an array of groups was returned.
      if (!empty($gs) && is_array($gs)) {
        // Rekey with the group type at the front.
        $gs = _usergroups_api_rekey_with_group_type($gtyp, $gs);
        $groups += $gs;
      }
    }
  }
  return $groups;
}

/**
 * Helper function to get a list of all possible groups.
 *
 * @param $gtype
 *   (optional) The specific group type to limit the groups to.
 *
 * @return
 *   If $gtype is passed in an array of all groups of that type,
 *   otherwise, an array of groups with each list of groups under
 *   the corresponding group title.
 */
function usergroups_api_get_all_group_options($gtype = NULL) {
  $groups = array();
  $gtypes =_usergroups_api_get_gtypes($gtype);
  foreach ($gtypes as $gtyp => $group) {
    // Don't include options for hidden groups.
    if (!$gtype && !empty($group['hidden'])) continue;
    $function = $group['module'] .'_usergroups_api_'. $gtyp .'_options';
    if (_usergroups_api_include_file($group, $function)) {
      $gs = $function();
      // Make sure an array of groups was returned.
      if (!empty($gs) && is_array($gs)) {
        // Rekey with the group type at the front.
        $gs = _usergroups_api_rekey_with_group_type($gtyp, $gs);
        if ($gtype) {
          $groups = $gs;
        }
        else {
          $groups[$group['title']] = $gs;
        }
      }
    }
  }
  return $groups;
}

/**
 * Determine the group title.
 *
 * @param $group_id
 *   The group id to get the title for.
 *
 * @return
 *   String that represents the title of the group.
 */
function usergroups_api_get_specific_group_title($group_id) {
  list($gtype, $gid) = _usergroups_api_split_key($group_id);
  if ($group = _usergroups_api_get_group_handlers($gtype)) {
    // Use the _options_title function if it is defined.
    $function = $group['module'] .'_usergroups_api_'. $gtype .'_options_title';
    if (_usergroups_api_include_file($group, $function)) {
      return $function($gid);
    }
    else {
      // Fallback - Load all of the options for the group.
      $function = $group['module'] .'_usergroups_api_'. $gtype .'_options';
      if (empty($group['hidden']) && _usergroups_api_include_file($group, $function)) {
        $groups = $function();
        // Return the title if present.
        return !empty($groups[$gid]) ? $groups[$gid] : '';
      }
    }
  }
  return '';
}

/**
 * Determine if a user is in a given group.
 *
 * @param $account
 *  The user object to check.
 * @param $group_id
 *   The group id to check against the user
 *
 * @return
 *   TRUE if the user is a member of the group, otherwise FALSE
 */
function usergroups_api_is_member($account, $group_id) {
  list($gtype, $gid) = _usergroups_api_split_key($group_id);
  if ($group = _usergroups_api_get_group_handlers($gtype)) {
    $function = $group['module'] .'_usergroups_api_'. $gtype .'_specific_groups';
    if (_usergroups_api_include_file($group, $function)) {
      $groups = $function($account);
      return !empty($groups[$gid]);
    }
  }
  return FALSE;
}

/**
 * Function for sorting an array of group ids. Use with custom sort functions.
 * Sorts by group type weight, then group type title.
 */
function usergroups_api_sort_group_ids($group_id1, $group_id2) {
  list($gtype1, $gid1) = _usergroups_api_split_key($group_id1);
  list($gtype2, $gid2) = _usergroups_api_split_key($group_id2);
  $one = $two = 0;
  if (($group1 = _usergroups_api_get_group_handlers($gtype1)) && isset($group1['weight'])) $one = $group1['weight'];
  if (($group2 = _usergroups_api_get_group_handlers($gtype2)) && isset($group2['weight'])) $two = $group2['weight'];
  if ($weight1 > $weight2) return 1;
  if ($weight2 > $weight1) return -1;
  $one = $two = 0;
  if (($group1) && isset($group1['weight'])) $one = $group1['title'];
  if (($group2) && isset($group2['weight'])) $two = $group2['title'];
  return strcmp($one, $two);
}

/**
 * Get a list of group types in the system.
 *
 * @param $gtype
 *   (optional) The id of the group type to limit the group
 *   types to.
 *
 * @return
 *   An array of group handlers currently defined in the
 *   system keyed by the group type id.
 */
function _usergroups_api_get_gtypes($gtype = NULL) {
  if ($gtype) {
    if ($group = _usergroups_api_get_group_handlers($gtype)) {
      $gtypes = array($gtype => $group);
    }
  }
  else {
    $gtypes = _usergroups_api_get_group_handlers();
  }
  return $gtypes;
}

/**
 * Get a list of group handlers. These are extended by modules
 * that implement hook_usergroups_api_groups().
 *
 * @param $gtype
 *   (optional) The id of the group type to get the handler for.
 *
 * @return
 *   Array handler if $gtype is passed in, otherwise an array of group
 *   handlers currently defined in the system keyed by the group type id.
 */
function _usergroups_api_get_group_handlers($gtype = NULL) {
  static $groups;
  if (!isset($groups)) {
    $groups = array(
      'user' => array(
        'name' => 'user',
        'title' => t('User'),
        'description' => t('The specific user. Each user is its own group.'),
        'include' => drupal_get_path('module', 'usergroups_api') .'/modules/user.usergroups.inc',
        'module' => 'user',
        'hidden' => TRUE,
        'weight' => -100,
      ),
      'role' => array(
        'name' => 'role',
        'title' => t('Role'),
        'description' => t('User is part of a group if they have the specific role.'),
        'description' => drupal_get_path('module', 'usergroups_api') .'/modules/user.usergroups.inc',
        'module' => 'user',
        'weight' => -99,
      ),
    );
    foreach (module_implements('usergroups_api_groups') as $module) {
      // Invoke the hook on the module.
      $results = module_invoke($module, 'usergroups_api_groups');
      // Make sure an array was returned.
      if (!empty($results) && is_array($results)) {
        // Go through each value.
        foreach ($results as $key => $result) {
          // Make sure an array was returned.
          if (is_array($result)) {
            // Make sure the name is defined.
            if (!isset($result['name'])) {
              $result['name'] = $key;
            }
            // Slam the module name in the array
            if (!isset($result['module'])) {
              $result['module'] = $module;
            }
            $groups[$result['name']] = $result;
          }
        }
      }
    }
  }

  if ($gtype !== NULL) return isset($groups[$gtype]) ? $groups[$gtype] : array();
  return $groups;
}

/**
 * Internal function to split a group_id into group type and internal group id.
 *
 * @param $key
 *   The group id to split.
 *
 * @return
 *   An array with two entries. Entry 0 is the $group_type, and 1 the internal
 *   group id.
 */
function _usergroups_api_split_key($key) {
  return explode('||', $key, 2);
}

/**
 * Rekey an array by adding $key_add to the front of each array key entry.
 * Used to add group type to the internal group id to get the group_id for
 * use by other modules.
 *
 * @param $key_add
 *   The string to append.
 * @param $array
 *   The array to rekey.
 *
 * @return array()
 *   $array is rekeyed with every entry of the passed in array having '$key_add||'
 *   appended to the old key: '$key_add||OLD KEY'.
 */
function _usergroups_api_rekey_with_group_type($key_add, $array) {
  $rekeyed  = array();
  foreach ($array as $key => $more) {
    $rekeyed[$key_add .'||'. $key] = $more;
  }
  return $rekeyed;
}

/**
 * Helper function to include a file and make sure a function exists.
 *
 * @param $handler
 *   An array that has $include_file_key as the key
 *   name that points to the name of a file to include in the system.
 * @param $function_name
 *   (optional) The name of the function that you plan to call after
 *   including the file.
 * @param $include_file_key
 *   (optional) The key name in $handler that points to the file
 *   to include
 *
 * @return bool
 *   If $function_name is passed in, then whether or not that function
 *   exists, otherwise, whether or not the include file was included. If there is
 *   no $include_file_key in $handler, then FALSE.
 */
function _usergroups_api_include_file($handler, $function_name = '', $include_file_key = 'include') {
  static $included = array('i' => array(), 'f' => array());

  // Include the file if it hasn't been included previously.
  if (isset($handler[$include_file_key]) && !isset($included['i'][$handler[$include_file_key]])) {
    $included['i'][$handler[$include_file_key]] = file_exists($handler[$include_file_key]) ? include_once($handler[$include_file_key]) : FALSE;
  }

  // Make sure that the function exists.
  if (!empty($function_name)) {
    if (!isset($included['f'][$function_name])) $included['f'][$function_name] = function_exists($function_name);
    return $included['f'][$function_name];
  }
  return !empty($included['i'][$handler[$include_file_key]]);
}
