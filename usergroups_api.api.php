<?php

/**
 * @file
 * Hooks provided by the Usergroups API module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Retrieve a list of groups defined.
 *
 * @return
 *   An array of associative arrays. Each inner array has elements:
 *   - "name": The internal name of the group. No more than 48 characters.
 *   - "title": The human-readable, localized name of the group.
 *   - "weight": An integer specifying the groups's sort ordering.
 *   - "description": (optional) A short description of what the group
 *      contains.
 *   - "include": (optional) The path to the file to include before calling
 *     additional hooks related to this group. Path should be relative to
 *     drupal root.
 *   - "hidden": (optional) Whether this group is hidden in lists and other
 *     places. If the list of groups is quite large, it may be a good idea
 *     to set this to TRUE.
 */
function hook_usergroups_api_groups() {
  return array(
    'user' => array(
      'name' => 'user',
      'title' => t('User'),
      'description' => t('The specific user. Each user is its own group.'),
      'include' => drupal_get_path('module', 'usergroups_api') .'/modules/user.usergroups.inc',
      'module' => 'user',
      'hidden' => TRUE,
      'weight' => -100,
    ),
    'role' => array(
      'name' => 'role',
      'title' => t('Role'),
      'description' => t('User is part of a group if they have the specific role.'),
      'description' => drupal_get_path('module', 'usergroups_api') .'/modules/user.usergroups.inc',
      'module' => 'user',
      'weight' => -99,
    ),
  );
}

/**
 * @return
 *   An associative array with each group defined by the module.
 *   The key should be the unique id and each element the group
 *   title.
 */
function hook_usergroups_api_GROUP_NAME_options() {
  $groups = array(
    1 => t('My cool group'),
    3 => t('My cool group 3'),
  );
  return $groups;
}

/**
 * Optional hook. If your module defines a large number of groups
 * or hook_usergroups_api_GROUP_NAME_options() takes a long time
 * to process, this hook will be called to determine the title
 * of a specific group.
 *
 * @param $id
 *   A group id defined by your module.
 * @return
 *   The title for a specific group.
 */
function hook_usergroups_api_GROUP_NAME_options_title($id) {
  if ($id == 1) {
    return t('My cool group');
  }
  else {
    return t('My cool group 3');
  }
}

/**
 * Determine the specific groups that a user belongs to.
 *
 * @param $account
 *   A user object to load the specific groups for.
 * @return
 *   An associative array of groups that the user is a member of.
 */
function hook_usergroups_api_GROUP_NAME_specific_groups($id) {
  $groups = array(
    1 => t('My cool group'),
  );
  return $groups;
}
