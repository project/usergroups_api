<?php

/**
 * @file
 * Usergroups API integration for user.module.
 */

/**
 * Implementation of hook_usergroups_api_options() for 'user'.
 * Ideally this function will never be called as it's going to load
 * every user in the database, but we must implement this function.
 */
function user_usergroups_api_user_options() {
  static $options;
  if (!is_array($options)) {
    $options = array();
    $results = db_query('SELECT u.* FROM {users} u WHERE u.status=1');
    foreach ($results as $r) {
      $options[$r->uid] = $r->name;
    }
  }
  return $options;
}

/**
 * Implementation of hook_usergroups_api_options_title() for 'user'.
 * Ideally this function will always be called.
 */
function user_usergroups_api_user_options_title($uid) {
  if ($r = db_query('SELECT u.* FROM {users} u WHERE u.uid=:uid', array(':uid' => $uid))->fetchObject()) return $r->name;
  return '';
}

/**
 * Implementation of hook_usergroups_api_specific_groups() for 'user'.
 */
function user_usergroups_api_user_specific_groups($account) {
  return array($account->uid => $account->name);
}

/**
 * Implementation of hook_usergroups_api_options() for 'role'.
 */
function user_usergroups_api_role_options() {
  static $roles;
  if (!is_array($roles)) {
    $roles = user_roles();
  }
  return $roles;
}

/**
 * Implementation of hook_usergroups_api_specific_groups() for 'role'.
 */
function user_usergroups_api_role_specific_groups($account) {
  return $account->roles;
  $roles = user_usergroups_api_role_options();
  $groups = array();
}
